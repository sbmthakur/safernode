#!/usr/bin/env python3

import json
import os
import re
import shutil
import subprocess
import sys
import tempfile
import yaml

homedir = os.path.expanduser('~')

docker_args = [
    '--user',       '{}:{}'.format(os.geteuid(), os.getegid()),
    '--workdir',    os.getcwd(),
    '--entrypoint', 'npm',
]

cmd_args = []
cleanup  = []
config   = {}

def main():

    # Forbid running as root
    if os.geteuid() == 0: fail("Not allowed to run as root")

    # Get the args for the npm command to run within docker
    cmd_args.extend(sys.argv[1:])

    run_npm()
    do_cleanup()

def run_npm():
    parse_config()

    # The npm command to run
    cmd = non_hyphenated_arg(cmd_args, 0)

    # We don't need to know about npm updates
    cmd_args.append('--no-update-notifier')

    cmd_args.extend(['--cache', '/home/node/.npm/'])

    if cmd in [None, 'version', 'help']:
        run_docker()
    elif cmd in ['init', 'create', 'innit']:
        run_npm_init()
    elif cmd in ['install', 'i', 'isntall', 'add']:
        run_npm_install()
    elif cmd in ['uninstall', 'un', 'unlink', 'remove', 'rm', 'r']:
        run_npm_uninstall()
    elif cmd in ['audit']:
        run_npm_audit()
    elif cmd in ['outdated']:
        run_npm_outdated()
    elif cmd in ['run', 'start', 'test']:
        run_npm_run()
    else:
        fail('Unsupported npm command: {}'.format(cmd))

def set_docker_networking():

    ports = list(
        map(lambda p: p if ':' in str(p) else '127.0.0.1:{0}:{0}'.format(p),
            config.get('ports', [])
        )
    )

    for port in ports:
        docker_args.extend(['-p', port])

    if len(ports) == 0 and 'network' not in config:
        docker_args.extend(['--net','none'])

def run_npm_audit():
    project_root_host = require_project_root()
    project_root_tmp  = mount_tmp_project_root()

    # node_modules
    mount(os.path.join(project_root_host, 'node_modules'), rw=True, create_dir=True)

    # Copy some files
    for name in ['package.json', 'package-lock.json']:
        copy_and_restore(name, project_root_host, project_root_tmp)

    run_docker()

def run_npm_outdated():
    project_root_host = require_project_root()

    # Some read-only mounts
    for name in ['package.json', 'package-lock.json', 'node_modules']:
        mount(os.path.join(project_root_host, name))

    run_docker()

def run_npm_init():

    # Before we do anything, check if the package.json exists. If it doesn't, then
    # we can assume that we are going to create it, and then make some small modifications
    # to it afterwards.
    pj_already_existed = os.path.exists('package.json')

    # Mount the current working directory read/write so npm can create the package.json in it
    mount(os.getcwd(), rw=True)

    run_docker()

    # If we created the package.json, set some safer defaults
    if not pj_already_existed:
        with open('package.json') as rfh:
            pj = json.load(rfh)
            if not 'private' in pj: pj['private'] = True
            rfh.close()
            with open('package.json', 'w') as wfh:
                json.dump(pj, wfh, indent=2)

def run_npm_run():
    project_root_host = require_project_root()
    project_root_tmp  = mount_tmp_project_root()

    # Configure networking
    set_docker_networking()

    # Things to mount
    mounts = config.get('mount', [])

    # Extract only paths
    mount_paths = map(lambda m: m.get('path'), mounts)
    if None in mount_paths:
        fail('Missing "path" attribute in config file mount declaration')

    # Default read-only mounts
    if not 'package.json' in mount_paths: mount(os.path.join(project_root_host, 'package.json'))
    if not 'node_modules' in mount_paths: mount(os.path.join(project_root_host, 'node_modules'))

    # User configured mounts
    for mp in mounts:

        path = os.path.join(project_root_host, mp.get('path'))
        rw = mp.get('rw', False)
        mount(path, rw=rw)

        # If the user specified a rw mount, and the path doesn't already exist, then it *may* get
        # created in the root rw tmpdir. In that case we want to persist that new file/dir, so we
        # recover it from the tmpdir before cleaning up
        if rw and not os.path.exists(path):
            newFile = os.path.join(project_root_tmp, mp.get('path'))
            cleanup.append(lambda: shutil.move(newFile, path) if os.path.exists(newFile) and not os.path.exists(path) else None)

    run_docker()

def run_npm_uninstall():
    run_npm_install()

def run_npm_install():
    project_root_host = require_project_root()
    project_root_tmp  = mount_tmp_project_root()

    # node_modules
    mount(os.path.join(project_root_host, 'node_modules'), rw=True, create_dir=True)

    for name in ['package.json', 'package-lock.json']:
        copy_and_restore(name, project_root_host, project_root_tmp)

    run_docker()

# Helper function: Copies a file into a tmpdir (which is mounted inside docker)
# and then creates a cleanup function to copy it back afterwards. This is needed
# because npm likes to create files and then move them rather than overwriting
# them in place, but assuming it wont cross a filesystem boundary.
def copy_and_restore(name, host_dir, tmp_dir):
    host_path = os.path.join(host_dir, name)
    tmp_path  = os.path.join(tmp_dir, name)
    if os.path.exists(host_path):
        shutil.copyfile(host_path, tmp_path)
    cleanup.append(lambda: shutil.move(tmp_path, host_path) if os.path.exists(tmp_path) else None)

def run_docker():
    proxy_env_to_docker()

    docker_image = get_docker_image()
    docker_cmd = 'docker run -i --rm'.split(' ')
    args = [
        'sudo',
        *docker_cmd,
        *docker_args,
        docker_image,
        *cmd_args
    ]

    if config.get('verbose', False):
        print_args = [
            'sudo',
            *map(lambda arg: red_text(arg), docker_cmd),
            *map(lambda arg: red_text(arg), docker_args),
            '\\\n      ',
            yellow_text(docker_image),
            '\\\n      ',
            *map(lambda arg: green_text(arg), cmd_args),
        ]
        print('$ {}'.format(' '.join(print_args)), file=sys.stderr)

    subprocess.run(args)

def proxy_env_to_docker():
    for k, v in os.environ.items():
        if k in ['HOME', 'USER'] or k.startswith('NODE_'):
            docker_args.extend([ '-e', '{}={}'.format(k, v) ])
        elif k.startswith('SAFERNODE_'):
            docker_args.extend([ '-e', '{}={}'.format(k[10:], v) ])

def find_project_root():
    dirname=os.getcwd()
    if os.path.exists(os.path.join(dirname, 'package.json')):
        return dirname

def require_project_root():
    project_root = find_project_root()
    if project_root is None:
        fail("Unable to find project root. Missing package.json in CWD")
    return project_root

def mount_tmp_project_root():
    project_root = require_project_root()
    path         = tempfile.mkdtemp()
    mount(path, dst=project_root, rw=True)
    cleanup.append(lambda: shutil.rmtree(path))
    return path

def parse_config():
    src = {}

    project_root = find_project_root()
    config_path  = None if project_root is None else os.path.join(project_root, '.safernode')

    if config_path is not None and os.path.exists(config_path):
        with open(config_path) as fh:
            src = yaml.load(fh)

    for k, v in src.items():
        config[k] = v

def non_hyphenated_arg(args, pos=0):
    args = list(filter(lambda a: not a.startswith('-'), args))
    if len(args) > pos:
        return args[pos]

def mount(src, dst=None, rw=False, create_dir=False):

    if dst == None:
        dst = src

    if not os.path.exists(src):
        if create_dir:
            os.makedirs(src)
        else:
            return

    mount = filter(lambda o: o is not None, [
        'type=bind',
        'readonly' if not rw else None,
        'source={}'.format(src),
        'destination={}'.format(dst)
    ])
    docker_args.extend([ '--mount', ','.join(mount) ])

def get_docker_image():

    node_version      = config.get('node_version', 'latest')
    docker_image_name = config.get('docker', {}).get('image', 'node:{}'.format(node_version))
    docker_image_file = config.get('docker', {}).get('build')

    # If an image file was supplied, then it's a local image. Check it exists and build if not
    if docker_image_file != None:
        
        # Check if image exists
        if not exists_docker_image(docker_image_name):
            if not docker_image_file.startswith('/'):
                project_root = find_project_root()
                docker_image_file = os.path.join(project_root, docker_image_file)

            build_docker_image(docker_image_name, docker_image_file)

    return docker_image_name

def exists_docker_image(name):
    docker_cmd = ['sudo', 'docker', 'images', '-qf', 'reference={}'.format(name)]
    output = subprocess.check_output(docker_cmd)
    return output != b''

def build_docker_image(name, file):
    print('* Building docker image: {} from {}'.format(name, file), file=sys.stderr)
    with open(file) as fh:
        docker_cmd = ['sudo', 'docker', 'build', '-qt', name, '.', '-f-']
        p = subprocess.Popen(docker_cmd, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        stderr = p.communicate(input=str.encode(fh.read()))[1]
        if stderr != None:
            fail(stderr)
        print('* Docker image built', file=sys.stderr)

def do_cleanup():
    while len(cleanup) > 0:
        cleanup.pop()()

def fail(err):
    print(err, file=sys.stderr)
    do_cleanup()
    sys.exit(1)

def coloured_text(colour, s):
    return '\033[{}m{}\033[0m'.format(colour, s)

def red_text(s):
    return coloured_text(91, s)

def green_text(s):
    return coloured_text(92, s)

def yellow_text(s):
    return coloured_text(33, s)

main()
