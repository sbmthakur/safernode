# SaferNode

## License

GPL-3.0. See the LICENSE file at the root of this project.

## Summary

NPM is a cesspit of interconnected, malicious, javascript libraries, ready to
install keyloggers, steal your SSH keys, API keys, and Bitcoin wallets, connect
you up to botnets and generally ruin your life, the moment you run "npm install"

Unfortunately, there is some useful code on NPM that I want to use, and I don't
want the inconvenience of manually isolating my projects by creating separate
user accounts for each of them, or firing up development VMs etc.

To that end, I wrote a python script which isolates npm inside short-lived
docker containers.

## Experimental

This is not a mature project. It's something I hacked together which I find
useful. I intend to make it better, but in the process expect to make backwards
incompatible changes. No doubt I've made decisions that aren't flexible enough
and need to be modified. You have been warned.

## Requirements

1. Docker
2. Python 3
3. A healthy distrust of NPM

## Installation

1. cd /usr/local/src/
2. sudo git clone https://gitlab.com/mikecardwell/safernode
3. cd safernode
4. sudo ln -s $(realpath safernode.py) /usr/local/bin/npm
5. Visit https://www.grepular.com/ and generously donate some money using one of
   of the links in the footer, Paypal/Bitcoin etc. After all, I probably just saved
   your ass.
6. Bookmark my blog, and subscribe to my RSS feed. You'd be crazy not to.

## Tell me more

You install my python script in your PATH as "npm". When that script is run, it
launches a docker container from an image containing the real node and npm, and
proxies the rest of your args through. Assuming your username is "mike" and uid
is "1000", and your current working directory is /home/mike/nodeproj, running the
command "npm start", will actually launch something similar to the following command:

```
sudo docker run -i --rm \
    --user       1000:1000 \
    --workdir    /home/mike/nodeproj \
    --entrypoint npm \
    --net        none \
    --mount type=bind,source=/tmp/tmpc0vhud50,destination=/home/mike/nodeproj \
    --mount type=bind,readonly,source=/home/mike/nodeproj/package.json,destination=/home/mike/nodeproj/package.json \
    --mount type=bind,readonly,source=/home/mike/nodeproj/node_modules,destination=/home/mike/nodeproj/node_modules \
    -e HOME=/home/mike \
    -e USER=mike \
       node:latest \
       start --no-update-notifier --cache /home/node/.npm/
```

There is a lot to unpack there. The things you're probably wondering about:

sudo

    Your user shouldn't have full access to dockerd. If it does, then you're basically
    running as root. My script assumes that you can only run docker by using sudo.

--user 1000:1000

    You run under the same UID inside the container as you're running on the host

--net none

    Network access is disabled by default. You can enable it in your .safernode. I'll get
    to that later.

--mount

    There are several things mounted into the container, some of which are mounted readonly.
    For various reasons, I sometimes mount temporary directories, which get cleaned
    up after running the container.

-e

    Some environment variables are proxied through. HOME, USER, and anything starting
    "NODE_". If you want to pass other environment variables in, prefix them with
    "SAFERNODE_". That prefix will be stripped before passing them through.

node:latest

    This is the docker image. It's the latest official dockerhub nodejs image. You can use
    a different image if you want. Again, you'll need to do that in .safernode and I'll explain
    later.

Of course, the above will fail as it only mounts the node_modules and package.json by default,
and not my actual code. Read the docs about the .safernode config file to discover how
to mount extra files and dirs inside the container

## .safernode config

Drop a ".safernode" yaml file at the root of your project if you need to tweak how
things run (you will need to). The following things can be tweaked:

### mount

This is only used when running an npm run/start/test command. It's an array of objects
containing a "path", and an optional "rw" boolean if it should be mounted rw. The
files/dirs are mounted relative to the project root. Example:

```yaml
mount:
- path: index.js
- path: lib
  rw: True
```

### network and ports

If your project needs to be able to access the network:

```yaml
network: True
```

If your project listens on some ports, you'll need to list them in your .safernode in order
to expose them to the host. E.g in order to forward port 3000:

```yaml
ports:
- 3000
```

This maps to the docker arg `-p 127.0.0.1:3000:3000`

If you need to do something clevererer, you can use the full docker format. E.g this
is equivalent:

```yaml
ports:
- 127.0.0.1:3000:3000
```

A side of effect of configuring ports is that "network" is set to "True". So you don't
need to specify both.

### verbose

If you want the docker command being run, to be printed to stderr:

```yaml
verbose: True
```

### node_version

If you want to use the "node:12" docker image, instead of "node:latest":

```yaml
node_version: 12
```

### docker

If you want to use a completely different docker image named "foobar/wibble":

```yaml
docker:
    image: foobar/wibble
```

If you want to build that docker image locally, drop a Dockerfile in your
project somwhere, and refer to it:

```yaml
docker:
    image: foobar/wibble
    build: Dockerfile
```

I found this useful for my "puppeteer" based projects, as the standard
node docker images couldn't launch chrome. I just created a Dockerfile
containing the following:

```
FROM node:12

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
 && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
 && apt-get update \
 && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont \
     --no-install-recommends \
 && rm -rf /var/lib/apt/lists/*
```

## Currently supported commands

### npm audit

@TODO: Write some docs

### npm init

Works as expected, except it merges `{"private": true}` into the package.json,
which I think is a more sensible default.

### npm install

@TODO: Write some docs

### npm outdated

@TODO: Write some docs

### npm run

@TODO: Write some docs

### npm start

@TODO: Write some docs

### npm uninstall

@TODO: Write some docs

### npm version

@TODO: Write some docs